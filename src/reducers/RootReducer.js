import { combineReducers } from 'redux';
import salaryReducer from './SalaryReducer';
export default combineReducers({
    salaryReducer
});