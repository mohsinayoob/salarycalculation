export default (state = {}, action) => {
    switch (action.type) {
     case 'SALARY_ACTION':
      return {
       result: action.payload
      }
     default:
      return state
    }
   }