export const salaryAction = (data) => dispatch => {
    data.name = data.firstName + ' ' + data.lastName;
    data.payPeriod = data.startDate.format("YYYY-MM-DD") + ' - ' +data.endDate.format("YYYY-MM-DD");
    data.grossIncome = Math.round(data.annualSalary/12);
    data.superRateIncome = Math.round(data.grossIncome/data.superRate);
    data.incomeTax = Math.round(calculateIncomeTax(data.annualSalary));
    data.netIncome = Math.round(data.grossIncome - data.incomeTax);
    dispatch({
     type: 'SALARY_ACTION',
     payload: data
    })
   }

   function calculateIncomeTax(annualSalary){
       if(annualSalary < 18201)
       {
            return 0;
       }
       else if(annualSalary > 18200 && annualSalary < 37001){
           return 18200*19/100;
       }
       else if(annualSalary > 37000 && annualSalary < 87001){
            return 3572 + 37000*32.5/100;
        }
        else if(annualSalary > 87000 && annualSalary < 180001){
            return 19822 + 87000*37/100;
        }
        else if(annualSalary > 180000 ){
            return 54232 + 180000*45/100;
        }
   }