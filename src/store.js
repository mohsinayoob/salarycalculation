import { createStore, applyMiddleware,compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/RootReducer';
import { persistStore } from 'redux-persist';

const store = createStore(
    rootReducer,
    undefined,
      applyMiddleware(thunk)
  );
  
  persistStore(store, { whitelist: ['salary'] });
  
  export default store; 