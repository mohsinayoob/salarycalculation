import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from '../logo.svg';
import '../styles/App.css';
import {Button} from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import '../styles/bootstrap/css/bootstrap.css';
import '../styles/main.css';
import '../styles/testimonials.css';
import '../styles/driverbankaccountsignup.css';
//import '../styles/driverbankaccountsignup.css';
import { salaryAction } from '../actions/salaryAction'
class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment(),
      endDate:moment(),
      firstName:'',
      lastName:'',
      annualSalary:'',
      superRate:'',
    };
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    
  }
 
  handleStartDateChange(date) {
    this.setState({
      startDate: date
    });
  }
  handleEndDateChange(date) {
    this.setState({
      endDate: date
    });
  }
  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
}
  handleSubmit(e)
  {
    e.preventDefault();
    if(this.state.firstName=='' || this.state.lastName=='' || this.state.annualSalary=='' || this.state.superRate=='' )
    { 
      alert("Please fill all the fields");
      return;
    }
    console.log(this.state);
    this.props.salaryAction(this.state);
  }
  componentWillMount() {
    
        this.setState({ rehydrated: true });
}
  salaryAction = (event) => {
    var data = {firstName:'', lastName:'', annualSalary:'',superRate:'', paymentDate:''}
    this.props.salaryAction(data);
   } 
  render() {
    return ( 
      <div className="App">
        <header className="App-header">
          
          <h1 className="App-title">Welcome to Salary Calculator</h1>
        </header>
        <div className="App-intro">
        <section className="small-section cws_prlx_section " style={{background:"#fbfbfb"}}>
        <div className="driver-center-sign-up-form">
              <div className="col-md-7 driver-center-sign-up-box">
                 
                <div className="sign-up-testimonial-item">
                    <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="registration-form">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="input-group form-input-group">
                                    <input className="form-control left-border-none form-input"
                                    placeholder="First Name" type="text" 
                                    ref="firstName" name="firstName" 
                                    onChange={(event) => this.handleUserInput(event)}
                                    style={{marginTop:"10px"}} />       
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="input-group form-input-group">
                                    <input className="form-control left-border-none form-input"
                                    placeholder="Lasr Name" type="text" 
                                    ref="lastName" name="lastName"
                                    onChange={(event) => this.handleUserInput(event)}                                    
                                    style={{marginTop:"20px"}} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="input-group form-input-group">
                                    <input className="form-control left-border-none form-input"
                                    placeholder="Annual Salary" type="number" 
                                    ref="annualSalary" name="annualSalary"
                                    onChange={(event) => this.handleUserInput(event)}                                    
                                    style={{marginTop:"20px"}} />     
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="input-group form-input-group">
                                    <input className="form-control left-border-none form-input"
                                    placeholder="Super Rate (%)" type="number" 
                                    ref="superRate" name="superRate"
                                    onChange={(event) => this.handleUserInput(event)}                                    
                                    style={{marginTop:"20px"}} />     
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="input-group form-input-group">
                                <DatePicker
                                className="form-control left-border-none form-input"
                                placeholder="Start Date" type="text" 
                                ref="startDate" name="startDate"
                                style={{marginTop:"20px"}}
                                 selected={this.state.startDate}
                                 onChange={this.handleStartDateChange}
                                 />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="input-group form-input-group">
                                <DatePicker
                                className="form-control left-border-none form-input"
                                placeholder="End Date" type="text" 
                                ref="endDate" name="endDate"
                                style={{marginTop:"20px"}}
                                 selected={this.state.endDate}
                                 onChange={this.handleEndDateChange}
                                 />
                                </div>
                            </div>
                        </div>
                        
                        <button className="btn btn-danger btn-round btn-lg btn-registration" 
                        type="submit" style={{marginTop:"40px", fontFamily:"Handel-Gothic-Reg"}}>Finish </button>
                        
                    </div>
                    </form>
              </div>
            </div>
           
        </div>
        <div className="" style={{marginLeft:'18%'}}>
            <div className="col-md-10 driver-center-sign-up-box">
                 <table border='1px'style={{width:'100%'}} >
                   <thead>
                     <tr>
                     <th> Name </th>
                     <th> Pay-Period </th>
                     <th> Gross Income </th>
                     <th> Income Tax </th>
                     <th> Net Income </th>
                     <th> Super Amount </th>
                     </tr>
                     </thead>
                     <tbody>
                     {this.props.salaryReducer.result ?
                       <tr>
                          
                         <td>{this.props.salaryReducer.result.name}</td>
                         <td>{this.props.salaryReducer.result.payPeriod}</td>
                         <td>{this.props.salaryReducer.result.grossIncome}</td>
                         <td>{this.props.salaryReducer.result.incomeTax}</td>
                         <td>{this.props.salaryReducer.result.netIncome}</td>
                         <td>{this.props.salaryReducer.result.superRateIncome}</td>
                         
                         </tr>
                         :
                         ''
                         }
                       </tbody>
                   </table>
              <div className="sign-up-testimonial-item">
                
              </div>
            </div>
          </div>
      </section>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  ...state
 })
 const mapDispatchToProps = dispatch => ({
  salaryAction: (data) => dispatch(salaryAction(data))
 })
 export default connect(mapStateToProps, mapDispatchToProps)(App);
