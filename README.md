
# Project Title

Salary Calculation

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.



### Installing



```
npm install
```


## Running the tests

```
npm run
```

### Test input

Explain what these tests test and why

```

first-name = Andrew last-name = Smith annual-salry = 60050 super-ratr = 9% 
```
